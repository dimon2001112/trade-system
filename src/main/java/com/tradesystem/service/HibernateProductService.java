package com.tradesystem.service;

import com.tradesystem.config.hibernate.HibernateUtil;
import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.entity.ProductEntity;
import com.tradesystem.model.Category;
import com.tradesystem.model.Product;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HibernateProductService implements ProductService {

    private final static Log log = LogFactory.getLog(HibernateProductService.class);

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();


    @Override
    public List<Product> findAll() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ProductEntity> cq = cb.createQuery(ProductEntity.class);
        cq.from(ProductEntity.class);
        List<ProductEntity> products = session.createQuery(cq).getResultList();
        transaction.commit();
        return products.stream().map(this::convertToProduct).collect(Collectors.toList());

    }



    private Product convertToProduct(ProductEntity productEntity) {
        Product product = new Product();
        product.setName(productEntity.getName());
        product.setDescription(productEntity.getDescription());
        product.setId(productEntity.getId());

        return product;
    }


    @Override
    public Product findById(long id) {
        return null;
    }

    @Override
    public Product save(Product product) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            CategoryEntity category = CategoryEntity.builder().id(product.getCategoryId()).build();
            var productEntity = ProductEntity.builder()
                    .name(product.getName())
                    .description(product.getDescription())
                    .category(category)
                    .build();
            session.persist(productEntity);
        } catch (Exception e ) {
            log.error("Error in Saving product", e );
            transaction.rollback();
        }
        transaction.commit();
        return product;
    }

    private Object convertToProductEntity(Product product) {
        return ProductEntity.builder()
                .name(product.getName())
                .description(product.getDescription())
                .build();
    }
    public void delete(Long id){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(
                CategoryEntity.builder()
                        .id(id)
                        .build()

        );
        transaction.commit();
    }

    @Service
    public static class HibernateCategoryService implements CategoryService {

        private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        @Override
        public List<CategoryEntity> findAll() {

            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
            cq.from(CategoryEntity.class);
            List<CategoryEntity> categories = session.createQuery(cq).getResultList();
            transaction.commit();
            return categories;

        }

        @Override
        public CategoryEntity findById(long id) {
            return null;
        }

        @Override
        public CategoryEntity save(CategoryEntity category) {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.persist(category);
            transaction.commit();
            return category;
        }

        private Object convertToCategoryEntity(Category category) {
            return Category.builder()
                    .name(category.getName())
                    .build();
        }
        public void delete(Long id){
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(
                    CategoryEntity.builder()
                            .id(id)
                            .build()

            );
            transaction.commit();
        }


    }
}
