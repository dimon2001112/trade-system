package com.tradesystem.service;

import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.model.Category;

import java.util.List;

public interface CategoryService {
    List<CategoryEntity> findAll();
    CategoryEntity findById(long id);
    CategoryEntity save(CategoryEntity category);


    void delete(Long id);
}
