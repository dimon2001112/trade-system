package com.tradesystem.service;

import com.tradesystem.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryProductService implements ProductService {

    private AtomicLong sequence = new AtomicLong(0);
    private Map<Long, Product> products = new HashMap<>();

    @Override
    public List<Product> findAll() {
        return new ArrayList<>(products.values());
    }

    @Override
    public Product findById(long id) {

        return products.get(id);
    }

    @Override
    public Product save(Product product) {
        product.setId(sequence.incrementAndGet());
        products.put(product.getId(), product);
        System.out.println("The product"+product+"has bean saved");
        return product;
    }
}
