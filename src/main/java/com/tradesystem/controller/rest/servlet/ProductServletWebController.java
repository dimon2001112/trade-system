package com.tradesystem.controller.rest.servlet;

import com.tradesystem.model.Product;
import com.tradesystem.service.HibernateProductService;
import com.tradesystem.service.InMemoryProductService;
import com.tradesystem.service.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

//@WebServlet("/web/product")
public class ProductServletWebController extends HttpServlet {

    private final ProductService productService = new HibernateProductService();
    @Override
    protected void doGet(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
        List<Product> products = productService.findAll();
        rq.setAttribute("products", products);
        RequestDispatcher rd = rq.getRequestDispatcher("/WEB-INF/jsp/product.jsp");
        rd.forward(rq, rs);
    }
    @Override
    protected void doPost(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
        String name = rq.getParameter("name");
        String description = rq.getParameter("description");
        Product product = Product.builder()
                .name(name)
                .description(description)
                .build();
        Product retunedProduct = productService.save(product);



        rs.sendRedirect("/web/product");


    }
}
