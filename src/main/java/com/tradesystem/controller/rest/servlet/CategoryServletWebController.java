package com.tradesystem.controller.rest.servlet;

import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.service.CategoryService;
import com.tradesystem.service.HibernateProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CategoryServletWebController extends HttpServlet {


    private final CategoryService categoryService = new HibernateProductService.HibernateCategoryService();

    @Override
    protected void doGet(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
        List<CategoryEntity> categories = categoryService.findAll();
        rq.setAttribute("categories", categories);
        RequestDispatcher rd = rq.getRequestDispatcher("/WEB-INF/jsp/category.jsp");
        rd.forward(rq, rs);
    }

    @Override
    protected void doPost(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
        String name = rq.getParameter("name");
        String description = rq.getParameter("description");
        CategoryEntity category = CategoryEntity.builder()
                .name(name)
                .build();
        CategoryEntity retunedCategory = categoryService.save(category);


        rs.sendRedirect("/web/category");


    }
}
