package com.tradesystem.controller.rest.servlet;

import com.tradesystem.model.Product;
import com.tradesystem.service.InMemoryProductService;
import com.tradesystem.service.ProductService;
import lombok.Builder;
import lombok.Getter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/product")
public class ProductServletRestController extends HttpServlet {
    private ProductService productService = new InMemoryProductService();

    @Override
    protected void doGet(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
        long  id = Long.parseLong(rq.getParameter("id"));
        Product product = productService.findById(id);
        rs.getWriter().println(product);

    }


}
