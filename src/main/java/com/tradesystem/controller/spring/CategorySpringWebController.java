package com.tradesystem.controller.spring;

import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("category")
@Controller
public class CategorySpringWebController  {
    private final CategoryService categoryService;

    public CategorySpringWebController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping
    public String findAll(Model model){
        List<CategoryEntity> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "category";
    }
    @PostMapping
    public  String save(CategoryEntity category) {
        categoryService.save(category);
        return "redirect:/web/category";
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        categoryService.delete(id);

    }

}
