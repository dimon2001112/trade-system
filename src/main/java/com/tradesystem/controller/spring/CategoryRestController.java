package com.tradesystem.controller.spring;

import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.entity.ProductEntity;
import com.tradesystem.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rest/category/")
public class CategoryRestController  {
    private final CategoryService categoryService;

    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @ResponseBody
    @PostMapping
    public CategoryEntity save( CategoryEntity categoryEntity ) {
        CategoryEntity category = CategoryEntity.builder()
                                                .name("Category 1")
                                                //.product(ProductEntity.builder().name("Product 1").build())
                                                //.product(ProductEntity.builder().name("Product 2").build())
                                                .build();



        return categoryService.save(category);
    }
}
