package com.tradesystem.controller.spring;

import com.tradesystem.model.Product;
import com.tradesystem.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("product")
public class ProductSpringWebController {
    private final ProductService productService;

    public ProductSpringWebController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping
    public String findAll(Model model) {
        List<Product> proucts = productService.findAll();
        model.addAttribute("products", proucts);
        return "product";

    }
    @PostMapping
    public String save(Product product) {
        productService.save(product);
        return "redirect:/web/product";
    }
}
