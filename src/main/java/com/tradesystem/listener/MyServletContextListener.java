package com.tradesystem.listener;

import com.tradesystem.controller.rest.servlet.CategoryServletWebController;
import com.tradesystem.controller.rest.servlet.ProductServletWebController;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
     //   sc.addServlet( "webProductControllerServlet", ProductServletWebController.class).addMapping("/web/product");
     //   sc.addServlet( "webCategoryControllerServlet", CategoryServletWebController.class).addMapping("/web/category");
    }
}
