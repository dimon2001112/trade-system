package com.tradesystem.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Entity
@Table(name = "categories")
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    //@OneToMany( cascade = CascadeType.ALL)
   // @JoinColumn(name = "category_id")
    //@Singular
    //private List<ProductEntity> products = new ArrayList<>();
}



