package com.tradesystem.config.servlet;

import com.tradesystem.controller.rest.servlet.HelloWorldServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class ServletContextInit implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        ServletRegistration.Dynamic sd = ctx.addServlet("helloWorldServlet", HelloWorldServlet.class);
        sd.addMapping("/hello");
    }
}