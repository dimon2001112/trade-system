package com.tradesystem.config.spring;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class WebTradeSystemConfig implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext webContext = new  AnnotationConfigWebApplicationContext();
        webContext.scan("com.tradesystem");
        ServletRegistration.Dynamic webRegistration = servletContext
                .addServlet("webDispatcher", new DispatcherServlet(webContext));
        webRegistration.addMapping("/web/*");
        webRegistration.setLoadOnStartup(1);




    }
}
