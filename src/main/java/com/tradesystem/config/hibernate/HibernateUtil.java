package com.tradesystem.config.hibernate;

import java.util.Properties;

import com.tradesystem.entity.CategoryEntity;
import com.tradesystem.entity.ProductEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    private static SessionFactory sessionFactory = build();

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private static SessionFactory build() {

        try {
            Configuration configuration = new Configuration();
            // Hibernate settings equivalent to hibernate.cfg.xml's properties
            Properties settings = new Properties();
            settings.put(Environment.STORAGE_ENGINE, "innodb");
            settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
            settings.put(Environment.URL, "jdbc:mysql://localhost:3306/tradesystem");
            settings.put(Environment.USER, "root");
            settings.put(Environment.PASS, "hhizp5565");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
            settings.put(Environment.SHOW_SQL, "true");
            settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
            settings.put(Environment.HBM2DDL_AUTO, "update");


/*            settings.put("hibernate.c3p0.min_size", "5");
            settings.put("hibernate.c3p0.max_size", "20");
            settings.put("hibernate.c3p0.timeout", "300");
            settings.put("hibernate.c3p0.max_statements", "50");
            settings.put("hibernate.c3p0.idle_test_period", "3000");*/

            configuration.setProperties(settings);

          configuration.addAnnotatedClass(ProductEntity.class);
            configuration.addAnnotatedClass(CategoryEntity.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sessionFactory;
    }

/*    private static StandardServiceRegistry standardServiceRegistry;
    private static SessionFactory sessionFactory;

    static {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        standardServiceRegistry = registryBuilder.configure().build();
        // Creating MetadataSources
        MetadataSources sources = new MetadataSources(standardServiceRegistry);
        // Creating Metadata
        Metadata metadata = sources.getMetadataBuilder().build();
        // Creating SessionFactory
        sessionFactory = metadata.getSessionFactoryBuilder().build();


    }
    //Utility method to return SessionFactory
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }*/
}