<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 30.11.2019
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Products</title>
    <link rel="stylesheet" href="<c:url value='/template/default/css/style.css' />"/>
</head>
<body>
Hello, ${username}<br><br>
<table>

The products:

        <th>id</th>
        <th>name</th>
        <th>description</th>
    </tr>
<c:forEach items="${products}" var="product">
    <tr>
        <td>${product.id}</td>
        <td>${product.name}</td>
        <td>${product.description}</td>
    </tr>
</c:forEach>
    </table>
<br><br>
<form method="post" action="/web/product">
    <input type="text" name="name"><br>
    <input type="text" name="description"><br>
    <select name="categoryId">
        <option value="1">Category 1</option>
        <option value="2">Category 2</option>
        <option value="3">Category 3</option>
    </select>
    <input type="submit">

    </form>
</body>
</html>
