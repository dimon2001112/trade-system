<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 09.01.2020
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Categories</title>
    <link rel="stylesheet" href="<c:url value='/template/default/css/style.css' />"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>
        function deleteCategory(id) {
            $.ajax({
                url: '/web/category/'+ id ,
                type: 'DELETE',
                success: function(result) {
                    window.location = '/web/category';
                },
                error: function (result) {
                    window.location = '/web/category';

                }
            });
        }
    </script>


</head>
<body>
    <table>

        The categories:

        <th>id</th>
        <th>name</th>

        <th>Actions</th>
        <c:forEach items="${categories}" var="category">
            <tr>
                <td>${category.id}</td>
                <td>${category.name}</td>
                <td><a href="javascript:deleteCategory(${category.id})">
                    delete
                </a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br><br>
    <form method="post" action="/web/category">
        <input type="text" name="name"><br>
        <input type="submit">

    </form>
</body>
</html>
